function read(element) {
    console.log("element: " + element)
    let data = {
        element: element,
        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val()
    }
    console.log(data)
    $.ajax({
        type: 'POST',
        url: 'update_status',
        data: data,
        success: function(response){
            console.log(response)
            $('#notifications').append(
                '<div class="card my-2 col-12 justify-content-center" data-id="' + response.notification.id + '"><div class="card-body"><b>' + response.notification.message + '</b> - ' + response.notification.status + '<button type="button" class="close float-right"><span>&times;</span></button></div></div>'
            )
            $('#'+response.notification.id).remove()
        }
    })
}