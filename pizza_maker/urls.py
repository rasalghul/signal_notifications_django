from django.urls import path
from . import views

urlpatterns = [
    path('create', views.create_pizza, name='create_pizza'),
    path('pizzas_available', views.list_pizzas, name='list_pizzas'),
    path('update_status', views.update_status, name='update_status'),
]
