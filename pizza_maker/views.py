from django.shortcuts import render, redirect
from django.forms.models import model_to_dict
from pizza_maker.models import *
from django.http import JsonResponse

# Create your pizzas here.
def create_pizza(request):
    # Save the new pizza into the database when POST
    if request.method == 'POST':
        data = request.POST.copy()
        new_pizza = Pizza()
        new_pizza.title = data.get('create_pizza_title')
        new_pizza.save()

        if new_pizza.id is not None:
            print("Yay!!! your pizza", new_pizza.title, " is ready. enjoy.")
            return redirect('list_pizzas')
        else:
            print("Your pizza is burned :(. Try again.")
            return redirect('create_pizza')
    return render(
        request,
        'create_pizza.html'
    )

# List all Pizzas availables
def list_pizzas(request):
    # Get only 5 notifications ordered by created_at
    notifications = Notifications.objects.all()[:5]
    context = {
        'notifications': notifications
    }
    return render(
        request,
        'list_pizzas.html',
        context
    )

def update_status(request):
    if request.method == 'POST':
        # Grab the element id and csrf token to allow post action
        data = request.POST.copy()
        id_notification = data.get('element')
        token = data.get('csrfmiddlewaretoken')
        # Change the status of the Notification
        notification = Notifications.objects.get(id=id_notification)
        notification.status = 'read'
        notification.save()
        # Return notification as a JSON response
        return JsonResponse({'notification': model_to_dict(notification)}, status=200)