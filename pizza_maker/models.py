from django.db import models
# Imports required for working wih signals
# This time we work with post_save callback method because we want to send user notification once 
from django.db.models.signals import post_save

# Create your models here.
class Pizza(models.Model):
    title = models.CharField(max_length=250, null=True)

    def __self__(self):
        return self.title

# Sender: in this case is the Pizza
# Instance: in this case is the object of the Pizza model (pizza that you are creating)
# kwargs: is for handle anything else is being passed
def pizza_save(sender, instance, **kwargs):
    msg = "Yey!!! " + instance.title + " is ready."
    status = "unread"
    notify = Notifications()
    notify.message = msg
    notify.status  = status
    notify.save()

# Link the reciver to the actual signal
post_save.connect(pizza_save, sender=Pizza)

class Notifications(models.Model):
    message = models.CharField(max_length=250, null=False)
    status  = models.CharField(max_length=6, null=True)